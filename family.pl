% Parent
% =====================================
% Queen Elizabeth II
parent(queenElizabeth, princeCharles).
parent(queenElizabeth, princessAnne).
parent(queenElizabeth, princeAndrew).
parent(queenElizabeth, princeEdward).
% Prince Phillip
parent(princePhillip, princeCharles).
parent(princePhillip, princessAnne).
parent(princePhillip, princeAndrew).
parent(princePhillip, princeEdward).
% Princess Diana
parent(princessDiana, princeWilliam).
parent(princessDiana, princeHarry).
% Prince Charles
parent(princeCharles, princeWilliam).
parent(princeCharles, princeHarry).
% Captain Mark Phillips
parent(captainMarkPhillips, peterPhillips).
parent(captainMarkPhillips, zaraPhillips).
% Princess Anne
parent(princessAnne, peterPhillips).
parent(princessAnne, zaraPhillips).
% Sarah Ferguson
parent(sarahFerguson, princessBeatrice).
parent(sarahFerguson, princessEugenie).
% Prince Andrew
parent(princeAndrew, princessBeatrice).
parent(princeAndrew, princessEugenie).
% Sophie Rhys-jones
parent(sophieRhysjones, jamesViscountSevern).
parent(sophieRhysjones, ladyLouiseMountbattenWindsor).
% Prince Edward
parent(princeEdward, jamesViscountSevern).
parent(princeEdward, ladyLouiseMountbattenWindsor).
% Prince William
parent(princeWilliam, princeGeorge).
parent(princeWilliam, theNewPrincess).
% Kate Middleton
parent(kateMiddleton, princeGeorge).
parent(kateMiddleton, theNewPrincess).
% Autumn Kelly
parent(autumnKelly, savannahPhillips).
parent(autumnKelly, islaPhillips).
% Peter Phillips
parent(peterPhillips, savannahPhillips).
parent(peterPhillips, islaPhillips).
% Zara Phillips
parent(zaraPhillips, miaGraceTindall).
% Mike Tindall
parent(mikeTindall, miaGraceTindall).
% =====================================

% Male
% =====================================
male(princePhillip).
male(princeCharles).
male(captainMarkPhillips).
male(timothyLaurence).
male(princeAndrew).
male(princeEdward).
male(princeWilliam).
male(princeHarry).
male(peterPhillips).
male(mikeTindall).
male(jamesViscountSevern).
male(princeGeorge).
% =====================================

% Female
% =====================================
female(queenElizabeth).
female(princessDiana).
female(camillaParkerBowles).
female(princessAnne).
female(sarahFerguson).
female(sophieRhysjones).
female(kateMiddleton).
female(autumnKelly).
female(zaraPhillips).
female(princessBeatrice).
female(princessEugenie).
female(ladyLouiseMountbattenWindsor).
female(theNewPrincess).
female(savannahPhillips).
female(islaPhillips).
female(miaGraceTindall).
% =====================================

% Married
% =====================================
% Queen Elizabeth - Prince Phillip
married(queenElizabeth, princePhillip).
married(princePhillip, queenElizabeth).
% Prince Charles - Camilla Parker Bowles
married(princeCharles, camillaParkerBowles).
married(camillaParkerBowles, princeCharles).
% Princess Anne - Timothy Laurence
married(princessAnne, timothyLaurence).
married(timothyLaurence, princessAnne).
% Sophie Rhys-jones - Prince Edward
married(sophieRhysjones, princeEdward).
married(princeEdward, sophieRhysjones).
% Prince William - Kate Middleton
married(princeWilliam, kateMiddleton).
married(kateMiddleton, princeWilliam).
% Autumn Kelly - Peter Phillips
married(autumnKelly, peterPhillips).
married(peterPhillips, autumnKelly).
% Zara Phillips - Mike Tindall
married(zaraPhillips, mikeTindall).
married(mikeTindall, zaraPhillips).
% =====================================

% Divorced
% =====================================
% Princess Diana - Prince Charles
divorced(princessDiana, princeCharles).
divorced(princeCharles, princessDiana).
% Captain Mark Phillips - Princess Anne
divorced(captainMarkPhillips, princessAnne).
divorced(princessAnne, captainMarkPhillips).
% Sarah Ferguson - Prince Andrew
divorced(sarahFerguson, princeAndrew).
divorced(princeAndrew, sarahFerguson).
% =====================================

% Rule predicates
% =====================================
% Father
father(Parent, Child):- male(Parent), parent(Parent, Child).
% Mother
mother(Parent, Child):- female(Parent), parent(Parent, Child).
% Child
child(Child, Parent):- parent(Parent, Child).
% Son
son(Child, Parent):- male(Child), child(Child, Parent).
% Daughter
daughter(Child, Parent):- female(Child), child(Child, Parent).
% Grandparent
grandparent(GrandParent, GrandChild):- parent(GrandParent, Parent), parent(Parent, GrandChild).
% Grandmother
grandmother(GrandMother, GrandChild):- female(GrandMother), grandparent(GrandMother, GrandChild).
% Grandfather
grandfather(GrandFather, GrandChild):- male(GrandFather), grandparent(GrandFather, GrandChild).
% Grandchild
grandchild(GrandChild, GrandParent):- grandparent(GrandParent, GrandChild).
% Grandson
grandson(GrandSon, GrandParent):- male(GrandSon), grandchild(GrandSon, GrandParent).
% Granddaughter
granddaughter(GrandDaughter, GrandParent):- female(GrandDaughter), grandchild(GrandDaughter, GrandParent).
% Spouse
spouse(Person1, Person2):- married(Person1, Person2), married(Person2, Person1).
% Husband
husband(Person, Wife):- male(Person), spouse(Person, Wife).
% Wife
wife(Person, Husband):- female(Person), spouse(Person, Husband).
% Sibling
sibling(Person1, Person2):- father(Father, Person1), father(Father, Person2), Person1 \== Person2.
% Brother
brother(Person, Sibling):- male(Person), sibling(Person, Sibling).
% Sister
sister(Person, Sibling):- female(Person), sibling(Person, Sibling).
% Aunt
aunt(Person, Child):- sister(Person, Parent), parent(Parent, Child).
% Uncle
uncle(Person, Child):- brother(Person, Parent), parent(Parent, Child).
% ChildOfSibling
childOfSibling(Child, Person):- aunt(Person, Child); uncle(Person, Child).
% Niece
niece(Child, Person):- female(Child), childOfSibling(Child, Person).
% Nephew
nephew(Child, Person):- male(Child), childOfSibling(Child, Person).
% First cousin
firstCousin(Person1, Person2):- child(Person1, Parent), childOfSibling(Person2, Parent).